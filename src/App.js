import React from 'react';
import Topo from './components/topo/topo';
import Main from './components/Main';
import Sidebar from './components/menu/Sidebar';
import Footer from './components/footer/Footer';

const App = () => (

    <div className="container-fluid">
        
            <Topo />
            <Sidebar />
            <Main />
            <Footer />
        
    </div>
)

export default App;
