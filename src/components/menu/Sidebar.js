import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = () => (

    <nav className="col col-sm-5 col-md-4 col-lg-3 navbar bg-dark">
        <ul className="nav flex-column">
            <li className="nav-item active">
                <Link to='/' className="nav-link" title="Dashboard">Dashboard</Link>
            </li>
            <li className="nav-item">
                <a href="#!" title="Cadastros">Cadastros</a>
                <div className="dropdown-menu">
                    <ul>
                        <li>
                            <a href="!#">Menu1</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li className="nav-item">
                <Link to='/configuracoes' className="nav-link" title="Configurações">Configurações</Link>
            </li>
        </ul>
    </nav>

);

export default Sidebar;