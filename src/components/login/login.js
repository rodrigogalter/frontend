import React, { Component } from 'react';

class Login extends Component {

    render() {

        return (
            <div className="row h-100 align-items-center justify-content-center">

                <div className="col col-6 col-md-4 login">

                    <form id="formLogin">

                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="email" name="email" id="email" className="form-control" placeholder="Email" aria-describedby="email" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="password">Senha</label>
                            <input type="password" name="password" id="password" className="form-control" placeholder="***" aria-describedby="senha" />
                        </div>

                        <button className="btn">Entrar</button>

                    </form>

                </div>
            </div>
        );
    }
}

export default Login;
