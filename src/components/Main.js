import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Dashboard from './dashboard/Dashboard'
import Sobre from './sobre/Sobre'
import Configuracoes from './configuracoes/Configuracoes'

const Main = () => (

    <Switch>
        <Route exact path='/' component={Dashboard} />
        <Route path='/sobre' component={Sobre} />
        <Route path='/configuracoes' component={Configuracoes} />
    </Switch>

)

export default Main
